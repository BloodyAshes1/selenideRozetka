package browserSetup;

import com.codeborne.selenide.Configuration;
import enums.Browsers;
import org.openqa.selenium.remote.DesiredCapabilities;


public class WebDriverFactory {

    static String browserFromJenkins = System.getenv("BROWSER");

    public static void initDriver(Browsers browser) {
    //    DesiredCapabilities capabilities = new DesiredCapabilities();
    //    Configuration.browserCapabilities = capabilities;
    //    Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.startMaximized = true;
        Configuration.timeout = 10000;
        Configuration.pageLoadTimeout = 20000;
        Configuration.browser = null;
     //   capabilities.setCapability("enableVNC", true);
       // capabilities.setCapability("sessionTimeout", "5m");

        if (browserFromJenkins != null) {
            Configuration.browser = browserFromJenkins;
        } else {
            switch (browser) {
                case EDGE: {
                    Configuration.browser = "edge";
                    return;
                }
                case CHROME: {
                    Configuration.browser = "chrome";
                    return;
                }
                case FIREFOX: {
                    Configuration.browser = "firefox";
                    return;
                }
                case SAFARI: {
                    Configuration.browser = "safari";
                }
            }
        }
    }

    public static void initDriver() {
        String browserName = System.getProperty("browserName", "chrome");
        try {
            initDriver(Browsers.valueOf(browserName.toUpperCase()));
        } catch (IllegalArgumentException e) {
            System.err.println("This browser is not supported!!!");
            System.exit(-1);
        }
    }
}
