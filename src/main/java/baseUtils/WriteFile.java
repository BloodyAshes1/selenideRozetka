package baseUtils;

import io.qameta.allure.Step;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;


public class WriteFile {

    private static final File products = new File("src/test/Smartphones.txt");

    @Step("Method writes titles and prices for products witch had been saved to {Map<String, String> map}")
    public static void writeToFile(Map<String, String> map) {
        try {
            FileWriter fileWriter = new FileWriter(products);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                fileWriter.write(String.valueOf(entry).replace("=", "-"));
            }
            System.out.println("File written: " + products.getName());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Step("Method returns titles of products from the file")
    public static File getProductsFile() {
        return products;
    }
}
