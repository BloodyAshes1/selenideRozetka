package baseUtils;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class BasePage {
    SelenideElement closeBanner(){
        return  $(".exponea-close-cross");
    }

    SelenideElement title(){
        return $("h1");
    }

    @Step("Method is used for closing PopUp banner")
    public void closeSideBanner(){
        closeBanner().click();
    }

    @Step("Method returns title for current page")
    public String getTitle(){
        return title().getText();
    }
}
