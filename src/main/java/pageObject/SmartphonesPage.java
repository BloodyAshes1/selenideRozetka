package pageObject;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.String.format;

public class SmartphonesPage {

    SelenideElement sortingField() {
        return $x("//rz-sort");
    }

    SelenideElement sortingValues(int valueId) {
        return sortingField().find(By.xpath(".//*[contains(@value, '" + valueId + "')]"));
    }

    ElementsCollection priceList(){
        return  $$x("//span[@class='goods-tile__price-value']");
    }

    SelenideElement preload() {
        return $(".preloader");
    }

    ElementsCollection productTitleWithAction() {
        return $$x("//*[@class='goods-tile__inner' and ./span[contains(text(),'АКЦИЯ')]]//span[@class='goods-tile__title']");
    }

    ElementsCollection productPricesWithAction() {
        return $$x("//*[@class='goods-tile__inner' and ./span[contains(text(),'АКЦИЯ')]]//span[@class='goods-tile__price-value']");
    }

    @Step("Method is used for switching pages in catalog")
    public SmartphonesPage switchPageByCount(int page) {
        $x("//*[contains(@class, 'pagination__item') and a[contains(text(),'" + page + "')]]").click();
        preload().shouldBe(not(visible));
        return  page(SmartphonesPage.class);
    }

    @Step("Method saves title and worth of product from the current page")
    public SmartphonesPage saveProductsTitleAndWorth(Map<String, String> map,int lastPageNumber) {
        for (int startPageNumber = 1; startPageNumber <= lastPageNumber; startPageNumber++) {
            int index = 0;
            while (index < productTitleWithAction().size()) {
                String title = productTitleWithAction().get(index).getText();
                String price = productPricesWithAction().get(index).getText();
                index++;
                map.put(format("%s ", title), format(" %s", price) + "\n");
            }
            if (startPageNumber < lastPageNumber) {
                switchPageByCount(startPageNumber + 1);
            }
        }
        return this;
    }

    @Step("Method for checking that products are sorting by price from the most expensive to the cheapest")
    public void checkSortingByPrice(boolean naturalOrder){
        List<Double> priceList = new ArrayList<>();
        for (SelenideElement element : priceList()){
            String priceText = element.getText().replaceAll(" ", "");
            System.out.println(priceText);
            double price = Double.parseDouble(priceText);
            priceList.add(price);
        }
        List<Double> sortedPrice = new ArrayList<>(priceList);
        if (naturalOrder) {
            Collections.sort(sortedPrice);
        } else {
            Collections.sort(sortedPrice, Collections.reverseOrder());
        }

    }

    @Step("Method for choosing type of sorting")
    public SmartphonesPage chooseSort(int value) {
        sortingValues(value).click();
        preload().shouldBe(not(visible));
        return  page(SmartphonesPage.class);
    }
}