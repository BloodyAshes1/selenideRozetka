package pageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class SmartphonesTVAndElectronicsPage {

    SelenideElement smartphonesCategory(){
        return $("a[href*='mobile-phones'].tile-cats__heading.tile-cats__heading_type_center");
    }

    @Step("Method navigates from Smartphones And TV page to Smartphones category ")
    public SmartphonesPage navigateToSmartphones(){
        smartphonesCategory().shouldBe(Condition.visible).click();
        return page(SmartphonesPage.class);
    }
}
