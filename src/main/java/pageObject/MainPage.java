package pageObject;

import baseUtils.BasePage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class MainPage extends BasePage {

    SelenideElement smartphonesAndTvCategory(){
        return $x("//a[contains(@href, 'telefony-tv-i-ehlektronika') and @class='menu-categories__link']");
    }
    @Step("Method navigates from start page to SmartphonesAndTV category")
    public SmartphonesTVAndElectronicsPage navigateToSmartphonesAndTV(){
        smartphonesAndTvCategory().shouldBe(Condition.visible).click();
        return page(SmartphonesTVAndElectronicsPage.class);
    }
}
