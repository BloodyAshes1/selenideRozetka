import baseUtils.WriteFile;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObject.MainPage;
import pageObject.SmartphonesPage;
import testsSetup.BaseTest;

import java.util.LinkedHashMap;
import java.util.Map;

public class SmartphonesTest extends BaseTest {
    Map<String, String> titleAndWorth = new LinkedHashMap<>();
    MainPage mainPage = new MainPage();
    SmartphonesPage smartphones = new SmartphonesPage();

    @BeforeClass
    public void beforeClass() {
        WriteFile.getProductsFile().delete();
    }


    @Owner(value = "Vlad Matsenko")
    @Description(value = "Test to verify that smartphone names and prices can be saved and stored in a file")
    @Test
    public void writeToFileSortedSmartphones() {
        mainPage.navigateToSmartphonesAndTV()
                .navigateToSmartphones()
                .chooseSort(2)
                .checkSortingByPrice(true);
        smartphones.saveProductsTitleAndWorth(titleAndWorth, 3);

        WriteFile.writeToFile(titleAndWorth);

        Assert.assertTrue(WriteFile.getProductsFile().exists());
    }
}