package testsSetup;

import browserSetup.WebDriverFactory;
import enums.Browsers;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.Listener;


import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;


@Listeners(Listener.class)
public class BaseTest {

    @BeforeSuite
    public void setUp(){
        WebDriverFactory.initDriver(Browsers.FIREFOX);
    }

    @BeforeMethod
    public void beforeMethod(){
        openUrl("https://rozetka.com.ua");
    }

    @AfterSuite
    public void afterSuite(){
        closeWebDriver();
    }

    public void openUrl(String url){
        open(url);
    }
}
